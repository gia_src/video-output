DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# delete previous output
rm -rf ${DIR}/_output

# create dirs
mkdir -p ${DIR}/_output
mkdir -p ${DIR}/_input

# compress video
for path in ${DIR}/_input/*; do
	file=$(basename "$path")
	name=${file%.*}

	# mp4
	${DIR}/bin/ffmpeg -y -i "${path}" -vcodec libx264 -pix_fmt yuv420p -profile:v baseline -preset slower -crf 18 -vf scale=-1:720 -b:v 750k -pass 1 -f mp4 /dev/null && \
	${DIR}/bin/ffmpeg -y -i "${path}" -vcodec libx264 -pix_fmt yuv420p -profile:v baseline -preset slower -crf 18 -vf scale=-1:720 -b:v 750k -pass 2 "${DIR}/_output/${name}.mp4"

	# webm
	${DIR}/bin/ffmpeg -y -i "${path}" -c:v libvpx -c:a libvorbis -pix_fmt yuv420p -b:v 750k -crf 26 -vf scale=-1:720 "${DIR}/_output/${name}.webm"
done

# cleanup
rm ffmpeg*
