# create a .mp4 or .webm video file from a .mov file

https://web.archive.org/web/20140710125834/https://blog.mediacru.sh/2013/12/23/The-right-way-to-encode-HTML5-video.html

## Installation

Just clone this repo. No need to faff about installing ffmpeg, there's a copy in `bin` (version 2.8.2, from [here](http://evermeet.cx/ffmpeg/)), with the necessary libraries already included.

## Set permissions

* `chmod u+x high-quality.command`
* `chmod u+x high-quality-no-audio.command`

## Usage

* Place high resolution files in `_input` folder
* run `bash [preset].command` or double-click on the appropriate file
* `.mp4` and `.webm` files will be written to `_output`

## Adjusting parameters

Just edit `create-mp4.sh` and `create-webm.sh` directly if you need different settings. Picking the right options is left as an exercise to the reader. (Start [here](https://ffmpeg.org/ffmpeg.html).)
