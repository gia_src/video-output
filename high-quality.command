DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# delete previous output
rm -rf ${DIR}/_output

# create dirs
mkdir -p ${DIR}/_output
mkdir -p ${DIR}/_input

# compress video
for path in ${DIR}/_input/*; do
	file=$(basename "$path")
	name=${file%.*}

	# mp4
	${DIR}/bin/ffmpeg -y -i "${path}" -c:v libx264 -pix_fmt yuv420p -profile:v baseline -preset veryslow -crf 25 -vf scale=-1:720 "${DIR}/_output/${name}.mp4"

	# webm
	${DIR}/bin/ffmpeg -y -i "${path}" -c:v libvpx -pix_fmt yuv420p -qmin 0 -qmax 63 -crf 10 -b:v 2M -vf scale=-1:720 -c:a libvorbis "${DIR}/_output/${name}.webm"
done

# cleanup
rm ffmpeg*
