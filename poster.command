DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# delete previous output
rm -rf ${DIR}/_output

# create dirs
mkdir -p ${DIR}/_output
mkdir -p ${DIR}/_input

# compress video
for path in ${DIR}/_input/*; do
	file=$(basename "$path")
	name=${file%.*}

	${DIR}/bin/ffmpeg -y -i "${path}" -vf "select=gte(n\,1)" -vframes 1 "${DIR}/_output/${name}.jpg"
done

# cleanup
rm ffmpeg*
